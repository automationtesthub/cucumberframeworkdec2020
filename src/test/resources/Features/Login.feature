@Login
Feature: Login Functionality

Background:
Given Browser should be opened


@sanity
Scenario: Valid Login
When user enters userid and password
And click on login button
Then Application should be navigated to home page
And Verify Logout link should exist on home page
And Browser close

@smoke
Scenario Outline: InValid Login
When user enters invalid userid "<Userid>" and password "<Password>"
And click on login button
Then Error message should appear on login page
And do not Verify Message 
And Browser close

Examples:
|Userid  | Password |
|abc     | bcd      |
|abc1    | bcd      |
|abc2    | bcd      |


@smoke123
Scenario: InValid Login steps
When enters crendentials from grid
|Seema | Bharati|
And click on login button
Then Error message should appear on login page
And Verify Message
And Browser close
