package com.vtiger.common;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.vtiger.stepdefinitions.baseTest;
import com.vtiger.stepdefinitions.login;


public class Methods {

	static WebDriverWait wait = new WebDriverWait(login.driver,30);	

	public static void entervalue(WebElement elm, String val,String msg) 
	{
		try
		{
			wait.until(ExpectedConditions.visibilityOf(elm));
			elm.clear();
			elm.sendKeys(val);
			//baseTest.logger.pass(val+" "+msg);
		}
		catch(Throwable t)
		{
			//baseTest.logger.fail("Error occured: "+t.getMessage());
			//baseTest.logger.info("Screenshot captured: <a href="+getScreenshot(baseTest.driver,"file")+">Screenshot</a>");
		}
	}

	public static void Click(WebElement elm,String msg)
	{
		try
		{
			wait.until(ExpectedConditions.elementToBeClickable(elm));		
			elm.click();
			//baseTest.logger.pass(msg);
		}
		catch(Throwable t)
		{
			//baseTest.logger.fail("Error occured:"+t.getMessage());
			//baseTest.logger.info("Screenshot captured: <a href="+getScreenshot(baseTest.driver,"file")+">Screenshot</a>");
		}
	}
	
	public static void VerifyTitle(String ExpectedTitle)
	{
		try
		{
			if(baseTest.driver.getTitle().equals(ExpectedTitle))
			{
			//baseTest.logger.pass("Title matched : "+ExpectedTitle);
			}
			else
			{
				//baseTest.logger.fail("Title did not match, Actual title is :"+baseTest.driver.getTitle());
			}
		}
		catch(Throwable t)
		{
			//baseTest.logger.fail("Error occured:"+t.getMessage());
			//baseTest.logger.info("Screenshot captured: <a href="+getScreenshot(baseTest.driver,"file")+">Screenshot</a>");
		}
	}
	
	public static void ElementExist(WebElement elm,String msg)
	{
		try
		{
			wait.until(ExpectedConditions.elementToBeClickable(elm));		
			elm.isDisplayed();
			//baseTest.logger.pass(msg);
		}
		catch(Throwable t)
		{
			//baseTest.logger.fail("Error occured:"+t.getMessage());
			//baseTest.logger.info("Screenshot captured: <a href="+getScreenshot(baseTest.driver,"file")+">Screenshot</a>");
		}
	}
	
	public static void VerifyAlertText(String Expalttext,String msg)
	{
		try
		{
			wait.until(ExpectedConditions.alertIsPresent());		
			String acttxt=baseTest.driver.switchTo().alert().getText();
			if(acttxt.equals(Expalttext))
			{
			//baseTest.logger.pass(msg);
			}
			else
			{
				//baseTest.logger.fail("Expected text was "+Expalttext+ "but found "+acttxt);	
			}
			baseTest.driver.switchTo().alert().accept();
		}
		catch(Throwable t)
		{
			//baseTest.logger.fail("Error occured:"+t.getMessage());
			//baseTest.logger.info("Screenshot captured: <a href="+getScreenshot(baseTest.driver,"file")+">Screenshot</a>");
		}
	}

	public static String getScreenshot(WebDriver driver, String screenshotName)  {
		//below line is just to append the date format with the screenshot name to avoid duplicate names		
	    String destination=null;
		try
		{
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		//after execution, you could see a folder "FailedTestsScreenshots" under src folder
		destination = System.getProperty("user.dir") + "/src/test/java/com/vtiger/reports/screenshot/"+screenshotName+dateName+".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//Returns the captured file path
		return destination;
	}
	
	public static Map<String, List<String>> ReadTestData(String ExcelPath, String Sheet)
	{
		Map<String,List<String>> map = new HashMap<String,List<String>>();
		//Xls_Reader xr = new Xls_Reader(ExcelPath);
		Xls_Reader xr = new Xls_Reader(ExcelPath);
		int rowcount = xr.getRowCount(Sheet);
		int colmcount= xr.getColumnCount(Sheet);
		System.out.println(rowcount+" "+colmcount);
		for(int i=2;i<=rowcount;i++)
		{
			
			String TCName= xr.getCellData(Sheet, "TestCaseName", i).trim();
			List<String> lst = new ArrayList<String>();
			for(int j=1;j<=colmcount;j++)
			{
				String colm=xr.getCellData(Sheet, j, i).trim();
				lst.add(colm);
			}
			
			map.put(TCName, lst);			
		}
		
		return map;
		
	}

}
