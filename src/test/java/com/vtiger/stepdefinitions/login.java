package com.vtiger.stepdefinitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.vtiger.pages.HomePage;
import com.vtiger.pages.LeadPage;
import com.vtiger.pages.LoginPage;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class login extends baseTest {
	
	
	@Given("Browser should be opened")
	public void browser_should_be_opened() {
		launchApp();
		logger = extent.createTest("TC01");
	  
	}

	@When("user enters userid and password")
	public void user_enters_userid_and_password() {
		LoginPage lp = new LoginPage(driver);
		lp.login("admin", "admin");
	}
	
	@When("user clicks on New Lead Link")
	public void user_clicks_on_New_Lead_Link() {
		HomePage hp = new HomePage(driver);
		hp.ClickNewLead();
		
	}

	@When("click on login button")
	public void click_on_login_button() {
		LoginPage lp = new LoginPage(driver);
		lp.clickLogin();
	}

	@Then("Application should be navigated to home page")
	public void application_should_be_navigated_to_home_page() {
		HomePage hp = new HomePage(driver);
		hp.verifyCalendar();
	    
	}

	@Then("Verify Logout link should exist on home page")
	public void verify_logout_link_should_exist_on_home_page() {
		HomePage hp = new HomePage(driver);
		hp.ValidateLinkLogout();
	}

	@When("user enters invalid userid and password")
	public void user_enters_invalid_userid_and_password() {
		LoginPage lp = new LoginPage(driver);
		lp.login("admin1", "admin1");		 
	}
	
	@When("enters crendentials from grid")
	public void enters_crendentials_from_grid(DataTable usercredentials) {
		List<List<String>> data = usercredentials.raw();		 
		 LoginPage lp = new LoginPage(driver);
		 lp.login(data.get(0).get(0), data.get(0).get(1));	
	}
	
	@When("^user enters invalid userid \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void user_enters_invalid_userid_and_password(String uid, String pwd) throws Throwable {
		 LoginPage lp = new LoginPage(driver);
		 lp.login(uid, pwd);	
	}

	@Then("Error message should appear on login page")
	public void error_message_should_appear_on_login_page() {
		LoginPage lp = new LoginPage(driver);
		 lp.validateErrorMsg();
	}

	@Then("Verify Message")
	public void verify_message() {
		LoginPage lp = new LoginPage(driver);
		 lp.validateErrorMsg();
	}
	
	@When("user enters lastname and company and clicks on save button")
	public void user_enters_lastname_company() {
		 LeadPage ldp = new LeadPage(driver);
		 ldp.CreateLeadWithMandatoryFields("modi", "BJP");
	}
	
	@Then("Browser close")
	public void Browser_close() {
		closeApp();
	}


}
