package com.vtiger.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.vtiger.common.Methods;


public class LoginPage {
		
	private WebDriver driver;
	
	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//By username = By.name("user_name");
	//By userpwd = By.name("user_password");
	//By login = By.name("Login");
	
	//WebElement elm = driver.findElement(By.name("user_name"));
	
	@FindBy(name="user_name")
	WebElement tb_username;
	
	
	@FindBy(xpath="//input[@name='user_password']")
	WebElement tb_userpwd;
	
	@FindBy(name="Login")
	WebElement btn_login;
	
	@FindBy(xpath="//img[@src='include/images/vtiger-crm.gif']")
	WebElement img_logo;
	
	@FindBy(xpath="//td[contains(text(),'You must specify a valid username and password.')]")
	WebElement msg_error;
	
	@FindBy(linkText="vtiger Customer Portal")
	WebElement lnk_VCP;
	
	
	
	
	
	
	
	
	public void login(String userid, String pwd)
	{
		Methods.entervalue(tb_username, userid,"entered in username field");
		Methods.entervalue(tb_userpwd, pwd,"entered in password field");
		//baseTest.logger.info("Screenshot captured: <a href="+Methods.getScreenshot(baseTest.driver,"Login")+">Screenshot</a>");
		//Methods.Click(btn_login,"clicked on login button");		
	}
	
	public void clickLogin()
	{
		Methods.Click(btn_login,"clicked on login button");	
	}
	
	public void validateLogo()
	{
		Methods.ElementExist(img_logo, "Logo exist on login page");
	}
	
	public void validateErrorMsg()
	{
		Methods.ElementExist(msg_error, "Error message verified");
	}
	public void validateLinkCustomerPortal()
	{
		Methods.ElementExist(lnk_VCP, "Customer Portal link verified");
	}
	
	public void validateTitle()
	{
		Methods.VerifyTitle("vtiger CRM - Commercial Open Source CRM");
	}
	
	public void verifyUsernameBox()
	{
		tb_username.isDisplayed();
		tb_username.isEnabled();
	}

}
