package com.vtiger.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.vtiger.common.Methods;

public class LeadPage extends HeaderPage {
	
private WebDriver driver;
	
	public LeadPage(WebDriver driver)
	{
		super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
   
	@FindBy(name="firstname")
	WebElement tb_firstname;
	
	@FindBy(name="lastname")
	WebElement tb_lastname;
	
	@FindBy(name="company")
	WebElement tb_company;
	
	@FindBy(name="button")
	WebElement btn_button;
	
	
	public void CreateLeadWithMandatoryFields(String lname, String comp)
	{
		Methods.entervalue(tb_lastname, lname, "text entered into lastname field");
		Methods.entervalue(tb_company, comp, "text entered into company field");
		Methods.Click(btn_button, "Save button clicked");
	}
	
	public void VerifyLastNameText(String lname)
	{
		WebElement elm = driver.findElement(By.xpath("//td[text()='Last Name:']/following::td[text()='"+lname+"']"));
		Methods.ElementExist(elm, lname+" exist against label Last Name:");
	}
	
	public void VerifyCompanyNameText(String comp)
	{
		WebElement elm = driver.findElement(By.xpath("//td[text()='Company:']/following::td[text()='"+comp+"']"));
		Methods.ElementExist(elm, comp+" exist against label Company:");
	}
	
	public void VerifyCreateLeadMandatoryFields(String lname, String comp)
	{
		Methods.Click(btn_button, "Save button clicked");
		Methods.VerifyAlertText("Last Name cannot be empty", "Alert Text validated");
		Methods.entervalue(tb_lastname, lname, "text entered into lastname field");
		Methods.Click(btn_button, "Save button clicked");
		Methods.VerifyAlertText("Company cannot be empty", "Alert Text validated");
		Methods.entervalue(tb_company, comp, "text entered into company field");
		Methods.Click(btn_button, "Save button clicked");
		
	}
	
	

}
