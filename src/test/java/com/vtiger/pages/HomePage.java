package com.vtiger.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.vtiger.common.Methods;

public class HomePage extends HeaderPage {
	
private WebDriver driver;
	
	public HomePage(WebDriver driver)
	{
		super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(id="theClockLayer")
	WebElement elm_theClockLayer;
	
	@FindBy(xpath="//table[@class='outer']")
	WebElement elm_Calendar;
	
	public void verifyClock()
	{
		Methods.ElementExist(elm_theClockLayer, "Clock is present on home page");
	}
	
	public void verifyCalendar()
	{
		Methods.ElementExist(elm_Calendar, "Calendar is present on home page");
	}
	
	

}
