package com.vtiger.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.vtiger.common.Methods;

public class HeaderPage {
	
private WebDriver driver;
	
	public HeaderPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Logout")
	WebElement lnk_Logout;
	
	@FindBy(linkText="My Account")
	WebElement lnk_MyAccount;
	
	@FindBy(linkText="Leads")
	WebElement lnk_Leads;
	
	@FindBy(linkText="New Lead")
	WebElement lnk_NewLead;
	
	public void ClickNewLead()
	{
		Methods.Click(lnk_NewLead, "Clicked on New Lead link");
	}
	
	public void ClickLeadsMenu()
	{
		Methods.Click(lnk_Leads, "Clicked on Leads Menu");
	}
	public void ClickLogout()
	{
		Methods.Click(lnk_Logout, "Clicked on Logout link");
	}
	
	public void ClickMyAccount()
	{
		Methods.Click(lnk_MyAccount, "Clicked on My Account link");
	}
	
	public void ValidateLinkLogout()
	{
		Methods.ElementExist(lnk_Logout, "Logout link exists");
	}

}
